# Projects gitstats

This project tracks the official forks of some projects abd generates
actual [gitstats](https://github.com/hoxu/gitstats) report for the latest
main branches.

## License

MIT

## Author

Tomasz Maczukin

