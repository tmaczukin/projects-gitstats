SHELL := /bin/bash

PROJECT_NAME ?= Test Project
PROJECT_URL ?= .git/
CI_JOB_NAME ?= test

TMP_PATH ?= tmp/$(CI_JOB_NAME)
TARGET_PATH ?= public/$(CI_JOB_NAME)
CONFIG_PATH ?= configs/$(CI_JOB_NAME).json
MAILMAP_FILE ?= mailmap

build:
	mkdir -p public/ tmp/ configs/
	test -f "$(TMP_PATH)" || rm -rf "$(TMP_PATH)"
	git clone "$(PROJECT_URL)" "$(TMP_PATH)"
	(test -f "$(MAILMAP_FILE)" && cp "$(MAILMAP_FILE)" "$(TMP_PATH)/.mailmap") || echo 'No mailmap file; skipping'
	gitstats -c project_name="$(PROJECT_NAME)" "$(TMP_PATH)" "$(TARGET_PATH)"
	cp gitstats.css "$(TARGET_PATH)/gitstats.css"
	find "$(TARGET_PATH)/" -name "*.dat" -delete
	find "$(TARGET_PATH)/" -name "*.plot" -delete
	echo '{"url":"$(PROJECT_URL)","name":"$(PROJECT_NAME)","path":"$(TARGET_PATH)"}' > "$(CONFIG_PATH)"

INDEX_FILE ?= public/index.html

index:
	cat index.html.top > "$(INDEX_FILE)"
	for project in configs/*; do \
		url=`jq -r <$$project .url`; \
		name=`jq -r <$$project .name`; \
		path=`jq -r <$$project .path`; \
		echo "<dt><a href=\"$$url\" target=\"_blank\">$$name</a></dt><dd><a href=\"$${path/public\//}\" target=\"_blank\">Gitstats</a></dd>" >> "$(INDEX_FILE)"; \
	done
	cat index.html.bottom >> "$(INDEX_FILE)"
	cp gitstats.css public/

